import { Component, OnInit, ViewEncapsulation } from '@angular/core';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  icon: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None      // ViewEncapsulation ======> change Headers Tabs color
})




export class DashboardComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {
  }

}
